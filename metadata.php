<?php
/*
 *   *********************************************************************************************
 *      Please retain this copyright header in all versions of the software.
 *      Bitte belassen Sie diesen Copyright-Header in allen Versionen der Software.
 *
 *      Copyright (C) Josef A. Puckl | eComStyle.de
 *      All rights reserved - Alle Rechte vorbehalten
 *
 *      This commercial product must be properly licensed before being used!
 *      Please contact info@ecomstyle.de for more information.
 *
 *      Dieses kommerzielle Produkt muss vor der Verwendung ordnungsgemäß lizenziert werden!
 *      Bitte kontaktieren Sie info@ecomstyle.de für weitere Informationen.
 *   *********************************************************************************************
 */

$sMetadataVersion   = '2.0';
$aModule            = [
    'id'            => 'ecs_liststar',
    'title'         => '<strong style="color:#04B431;">e</strong><strong>ComStyle.de</strong>:  <i>ListStar</i>',
    'description'   => 'Anzeige von Bewertungssternen in der Listenansicht.',
    'thumbnail'     => 'ecs.png',
    'version'       => '1.0.2',
    'author'        => '<strong style="font-size: 17px;color:#04B431;">e</strong><strong style="font-size: 16px;">ComStyle.de</strong>',
    'email'         => 'info@ecomstyle.de',
    'url'           => 'https://ecomstyle.de',
    'extend' => [
    ],
    'blocks' => [
        ['template' => 'widget/product/listitem_infogrid.tpl',  'block' => 'widget_product_listitem_infogrid_price',    'file' => '/views/blocks/liststar.tpl', 'position' => '9999'],
        ['template' => 'widget/product/listitem_grid.tpl',      'block' => 'widget_product_listitem_grid_price',        'file' => '/views/blocks/liststar.tpl', 'position' => '9999'],
        ['template' => 'widget/product/listitem_line.tpl',      'block' => 'widget_product_listitem_line_price',        'file' => '/views/blocks/liststar.tpl', 'position' => '9999'],
    ],
];
