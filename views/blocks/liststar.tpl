[{*
  *   *********************************************************************************************
  *      Please retain this copyright header in all versions of the software.
  *      Bitte belassen Sie diesen Copyright-Header in allen Versionen der Software.
  *
  *      Copyright (C) Josef A. Puckl | eComStyle.de
  *      All rights reserved - Alle Rechte vorbehalten
  *
  *      This commercial product must be properly licensed before being used!
  *      Please contact info@ecomstyle.de for more information.
  *
  *      Dieses kommerzielle Produkt muss vor der Verwendung ordnungsgemäß lizenziert werden!
  *      Bitte kontaktieren Sie info@ecomstyle.de für weitere Informationen.
  *   *********************************************************************************************
  *}]

[{$smarty.block.parent}]

[{if !isset($oConfig)}]
  [{assign var="oConfig" value=$oViewConf->getConfig()}]
[{/if}]

[{if $oConfig->getConfigParam('bl_perfLoadReviews')}]
    <style type="text/css">
    .liststar.star-ratings .fa-star, .liststar.star-ratings .fa-star-half-o {
        font-size: 22px;
    }
    .liststar.star-ratings .rating-star-empty {
        color: #c7c7c7;
    }
    .liststar.star-ratings .rating-star-filled {
        color: #fb0;
    }
    .liststar.ratingcount {
        vertical-align: top;
    }
    </style>
    <div class="liststar star-ratings[{if $type == 'grid'}] text-center[{/if}] [{$type}]">
        [{assign var="iRatingValue" value=$product->oxarticles__oxrating->value }]
        [{section name="starRatings" start=0 loop=5}]
            [{if $iRatingValue == 0}]
                <i class="fa fa-star rating-star-empty"></i>
            [{else}]
                [{if $iRatingValue > 1}]
                    <i class="fa fa-star rating-star-filled"></i>
                    [{assign var="iRatingValue" value=$iRatingValue-1}]
                [{else}]
                    [{if $iRatingValue < 0.5}]
                        [{if $iRatingValue < 0.3}]
                            <i class="fa fa-star rating-star-empty"></i>
                        [{else}]
                            <i class="fa fa-star-half-o rating-star-filled"></i>
                        [{/if}]
                        [{assign var="iRatingValue" value=0}]
                    [{elseif $iRatingValue > 0.8}]
                        <i class="fa fa-star rating-star-filled"></i>
                        [{assign var="iRatingValue" value=0}]
                    [{else}]
                        <i class="fa fa-star-half-o rating-star-filled"></i>
                        [{assign var="iRatingValue" value=0}]
                    [{/if}]
                [{/if}]
            [{/if}]
        [{/section}]

        [{assign var="iRatingCount" value=$product->getArticleRatingCount($oConfig->getConfigParam('blShowVariantReviews'))}]
        [{if $iRatingCount}]
            <a class="liststar ratingcount" href="[{$_productLink}]#review">
        [{/if}]
                <span class="liststar ratingcount">([{$iRatingCount}])</span>
        [{if $iRatingCount}]
            </a>
        [{/if}]

    </div>
[{/if}]
